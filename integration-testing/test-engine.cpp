// cmake-cache : A C++ library for reading and writing CMake cache variables.
//
// Copyright (C) 2018  Taylor Holberton
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include <cmake/cache/engine.hpp>
#include <cmake/cache/variable.hpp>

#include <gtest/gtest.h>

#include <memory>

namespace cmake {

namespace cache {

namespace integration_testing {

TEST(EngineTest, TestReadCache) {

	std::unique_ptr<Engine> engine(Engine::Create());

	engine->ReadCache(R"(
// This is a
// variable description.
TEST_VARIABLE:PATH=/usr/include

// Another description.
TEST_VARIABLE_2:BOOL=ON
	)");

	auto variable = engine->Find("TEST_VARIABLE");

	auto variableHasValue = !!variable;
	ASSERT_EQ(variableHasValue, true);
	EXPECT_EQ(variable->GetDescription(), " This is a variable description.");
	EXPECT_EQ(variable->GetValue(), "/usr/include");
	EXPECT_EQ(variable->GetType(), Variable::Type::DirectoryPath);

	variable = engine->Find("TEST_VARIABLE_2");

	variableHasValue = !!variable;
	ASSERT_EQ(variableHasValue, true);
	EXPECT_EQ(variable->GetDescription(), " Another description.");
	EXPECT_EQ(variable->GetValue(), "ON");
	EXPECT_EQ(variable->GetType(), Variable::Type::Bool);

	auto invalidVariable = engine->Find("INVALID_VARIABLE");

	auto invalidVariableHasValue = !!invalidVariable;

	EXPECT_EQ(invalidVariableHasValue, false);
}

TEST(EngineTest, TestWriteCache) {

	std::unique_ptr<Engine> engine(Engine::Create());

	engine->DefineBool("example_2", false);
	engine->DefineString("example_1", "some\nvalue");
	engine->Define("example_3", Variable::Type::Internal, "internal_value_1");
	engine->Define("example_4", Variable::Type::FilePath, "/usr/include");
	engine->Define("example_5", Variable::Type::Internal, "internal_value_2");

	std::ostringstream stringStream;

	engine->WriteCache(stringStream);

	const char expectedString[] = "########################\n"
	                              "# EXTERNAL cache entries\n"
	                              "########################\n"
	                              "\n"
	                              "example_1:STRING=some\\nvalue\n"
	                              "\n"
	                              "example_2:BOOL=OFF\n"
	                              "\n"
	                              "example_4:FILEPATH=/usr/include\n"
	                              "\n"
	                              "########################\n"
	                              "# INTERNAL cache entries\n"
	                              "########################\n"
	                              "\n"
	                              "example_3:INTERNAL=internal_value_1\n"
	                              "example_5:INTERNAL=internal_value_2\n";

	EXPECT_EQ(stringStream.str(), expectedString);
}

} // namespace integration_testing

} // namespace cache

} // namespace cmake
