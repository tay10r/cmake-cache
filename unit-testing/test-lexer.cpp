// cmake-cache : A C++ library for reading and writing CMake cache variables.
//
// Copyright (C) 2018  Taylor Holberton
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include <gtest/gtest.h>

#include <cmake/cache/lexer.hpp>
#include <cmake/cache/string-reader.hpp>
#include <cmake/cache/token.hpp>
#include <cmake/cache/token-consumer.hpp>

#include <memory>

namespace cmake {

namespace cache {

namespace unit_testing {

template <typename Callable>
class CallableTokenConsumer final : public TokenConsumer {
	Callable callable;
public:
	CallableTokenConsumer(Callable callable_) : callable(callable_) {

	}
	~CallableTokenConsumer() {

	}
	void Consume(const Token &token) override {
		callable(token);
	}
};

template <typename Callable>
bool RunTest(const char *string, Callable callable) {

	CallableTokenConsumer<Callable> tokenConsumer(callable);

	StringReader stringReader(string);

	std::unique_ptr<Lexer> lexer(Lexer::Create());
	lexer->SetReader(stringReader);
	lexer->SetTokenConsumer(tokenConsumer);
	return lexer->Scan();
}

TEST(LexerTest, ScanIdentifier) {

	auto tokenFound = RunTest("id_1", [](const Token &token) {
		EXPECT_EQ(token.GetType(), Token::Type::Identifier);
		EXPECT_EQ(token.GetData(), "id_1");
	});

	EXPECT_EQ(tokenFound, true);
}

TEST(LexerTest, ScanSpace) {

	auto tokenFound = RunTest("\t ", [](const Token &token) {
		EXPECT_EQ(token.GetType(), Token::Type::Space);
		EXPECT_EQ(token.GetData(), "\t ");
	});

	EXPECT_EQ(tokenFound, true);
}

TEST(LexerTest, ScanNewline) {

	auto tokenFound = RunTest("\n", [](const Token &token) {
		EXPECT_EQ(token.GetType(), Token::Type::Newline);
		EXPECT_EQ(token.GetData(), "\n");
	});

	EXPECT_EQ(tokenFound, true);
}

TEST(LexerTest, ScanNewlineWithCarriageReturn) {

	auto tokenFound = RunTest("\r\n", [](const Token &token) {
		EXPECT_EQ(token.GetType(), Token::Type::Newline);
		EXPECT_EQ(token.GetData(), "\r\n");
	});

	EXPECT_EQ(tokenFound, true);
}

TEST(LexerTest, ScanColon) {

	auto tokenFound = RunTest(":", [](const Token &token) {
		EXPECT_EQ(token.GetType(), Token::Type::Colon);
		EXPECT_EQ(token.GetData(), ":");
	});

	EXPECT_EQ(tokenFound, true);
}

TEST(LexerTest, ScanEqualSign) {

	auto tokenFound = RunTest("=", [](const Token &token) {
		EXPECT_EQ(token.GetType(), Token::Type::EqualSign);
		EXPECT_EQ(token.GetData(), "=");
	});

	EXPECT_EQ(tokenFound, true);
}

TEST(LexerTest, ScanComment) {

	auto tokenFound = RunTest("# a comment\n", [](const Token &token) {
		EXPECT_EQ(token.GetType(), Token::Type::Comment);
		EXPECT_EQ(token.GetData(), "# a comment");
	});

	EXPECT_EQ(tokenFound, true);
}

TEST(LexerTest, ScanDescriptionLine) {

	auto tokenFound = RunTest("// A variable description.\n", [](const Token &token) {
		EXPECT_EQ(token.GetType(), Token::Type::DescriptionLine);
		EXPECT_EQ(token.GetData(), "// A variable description.");
	});

	EXPECT_EQ(tokenFound, true);
}

TEST(LexerTest, ScanIncompleteDescriptionLine) {

	auto tokenFound = RunTest("/", [](const Token &token) {
		EXPECT_EQ(token.GetType(), Token::Type::Other);
		EXPECT_EQ(token.GetData(), "/");
	});

	EXPECT_EQ(tokenFound, true);
}

TEST(LexerTest, ScanOther) {

	auto tokenFound = RunTest("\xCE\x94", [](const Token &token) {
		EXPECT_EQ(token.GetType(), Token::Type::Other);
		/// Capital delta sign
		EXPECT_EQ(token.GetData(), "\xCE\x94");
	});

	EXPECT_EQ(tokenFound, true);
}

TEST(LexerTest, ScanInvalidUTF8LeadingByte) {

	auto tokenFound = RunTest("\xff", [](const Token &token) {
		EXPECT_EQ(token.GetType(), Token::Type::Other);
		/// Replaced with the 'replacement character'
		EXPECT_EQ(token.GetData(), "\xEF\xBF\xBD");
	});

	EXPECT_EQ(tokenFound, true);
}

} // namespace unit_testing

} // namespace cache

} // namespace cmake
