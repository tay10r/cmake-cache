// cmake-cache : A C++ library for reading and writing CMake cache variables.
//
// Copyright (C) 2018  Taylor Holberton
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "fake-lexer.hpp"

#include <cmake/cache/token.hpp>
#include <cmake/cache/token-consumer.hpp>

#include <vector>

namespace cmake {

namespace cache {

namespace unit_testing {

namespace {

namespace impl {

class FakeToken final : public Token {
	std::string data;
	Token::Type type;
	size_t line;
	size_t column;
public:
	FakeToken(const std::string &data_, Token::Type type_) {
		data = data_;
		type = type_;
		line = 1;
		column = 1;
	}
	~FakeToken() {

	}
	Token *Copy() const override {
		return new FakeToken(*this);
	}
	const std::string &GetData() const noexcept override {
		return data;
	}
	Token::Type GetType() const noexcept override {
		return type;
	}
	size_t GetLine() const noexcept override {
		return line;
	}
	size_t GetColumn() const noexcept override {
		return column;
	}
};

class FakeLexer final : public unit_testing::FakeLexer {
	std::vector<FakeToken> tokenVec;
	size_t index;
	TokenConsumer *consumer;
public:
	FakeLexer() : index(0), consumer(nullptr) {

	}
	~FakeLexer() {

	}
	bool Scan() override {

		if (consumer == nullptr) {
			return false;
		}

		if (index >= tokenVec.size()) {
			return false;
		}

		consumer->Consume(tokenVec[index]);

		index++;

		return true;
	}
	void PushToken(const std::string &data, Token::Type type) override {
		tokenVec.emplace_back(data, type);
	}
	void SetTokenConsumer(TokenConsumer &consumer_) override {
		consumer = &consumer_;
	}
	void SetReader(Reader &) override {

	}
};

} // namespace impl

} // namespace

FakeLexer *FakeLexer::Create() {
	return new impl::FakeLexer;
}

} // namespace unit_testing

} // namespace cache

} // namespace cmake
