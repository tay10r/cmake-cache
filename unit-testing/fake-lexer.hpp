// cmake-cache : A C++ library for reading and writing CMake cache variables.
//
// Copyright (C) 2018  Taylor Holberton
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef CMAKE_CACHE_UNIT_TESTING_FAKE_LEXER_HPP
#define CMAKE_CACHE_UNIT_TESTING_FAKE_LEXER_HPP

#include <cmake/cache/lexer.hpp>
#include <cmake/cache/token.hpp>

namespace cmake {

namespace cache {

namespace unit_testing {

class FakeLexer : public Lexer {
public:
	static FakeLexer *Create();
	virtual ~FakeLexer() { }
	virtual void PushToken(const std::string &data, Token::Type type) = 0;
};

} // namespace unit_testing

} // namespace cache

} // namespace cmake

#endif // CMAKE_CACHE_UNIT_TESTING_FAKE_LEXER_HPP
