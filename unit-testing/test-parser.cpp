// cmake-cache : A C++ library for reading and writing CMake cache variables.
//
// Copyright (C) 2018  Taylor Holberton
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include <gtest/gtest.h>

#include <cmake/cache/parser.hpp>
#include <cmake/cache/node.hpp>
#include <cmake/cache/node-consumer.hpp>
#include <cmake/cache/node-visitor.hpp>
#include <cmake/cache/variable-decl.hpp>

#include "fake-lexer.hpp"

#include <memory>

namespace cmake {

namespace cache {

namespace unit_testing {

template <typename Callable>
class CallableVariableDeclChecker final : public NodeConsumer, public NodeVisitor {
	Callable callable;
public:
	CallableVariableDeclChecker(Callable callable_) : callable(callable_) {

	}
	~CallableVariableDeclChecker() {

	}
protected:
	void Consume(const Node &node) override {
		node.Accept(*this);
	}
	void Visit(const VariableDecl &variableDecl) override {
		callable(variableDecl);
	}
	void Visit(const UnusedToken &) override {

	}
	void Visit(const UnexpectedToken &) override {

	}
};

template <typename Callable>
bool RunVariableDeclTest(Lexer &lexer, Callable callable) {

	CallableVariableDeclChecker<Callable> checker(callable);

	std::unique_ptr<Parser> parser(Parser::Create());
	parser->SetLexer(lexer);
	parser->SetNodeConsumer(checker);
	return parser->Parse();
};

TEST(ParserTest, ParseVariableDecl) {

	std::unique_ptr<FakeLexer> fakeLexer(FakeLexer::Create());
	fakeLexer->PushToken("// This is", Token::Type::DescriptionLine);
	fakeLexer->PushToken("\n", Token::Type::Newline);
	fakeLexer->PushToken("// a description.", Token::Type::DescriptionLine);
	fakeLexer->PushToken("\n", Token::Type::Newline);
	fakeLexer->PushToken("var_name", Token::Type::Identifier);
	fakeLexer->PushToken(":", Token::Type::Colon);
	fakeLexer->PushToken("BOOL", Token::Type::Identifier);
	fakeLexer->PushToken("=", Token::Type::EqualSign);
	fakeLexer->PushToken("OFF", Token::Type::Identifier);
	fakeLexer->PushToken("\n", Token::Type::Newline);

	auto nodeFound = RunVariableDeclTest(*fakeLexer, [](const VariableDecl &variableDecl) {
		EXPECT_EQ(variableDecl.GetVariableName().GetData(), "var_name");
		EXPECT_EQ(variableDecl.GetTypeName().GetData(), "BOOL");
	});

	EXPECT_EQ(nodeFound, true);
}

} // namespace unit_testing

} // namespace cache

} // namespace cmake
