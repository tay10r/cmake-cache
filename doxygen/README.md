Documentation
=============

Welcome to the cmake-cache documentation site!

The cmake-cache project is a project intended on making it easy to access CMake cache files, as well as providing interfaces for more complex applications.

The easiest way to get started with the library is with the @ref cmake::cache::Engine class.

The main purpose of the @ref cmake::cache::Engine class is for providing ease of use.

To get started using this class, use the @ref cmake::cache::Engine::Create method.

```{.cxx}
#include <cmake/cache/engine.hpp>

int main() {

	auto engine = cmake::cache::Engine::Create();

	return 0;
}
```

This method will created a new engine instance.

Since we'll have to free this instance later on, it may be easier to use a smart pointer.

```{.cxx}
#include <cmake/cache/engine.hpp>

#include <memory>

int main() {

	std::unique_ptr<cmake::cache::Engine> engine(cmake::cache::Engine::Create());

	return 0;
}
```

Once the engine is created, we can use @ref cmake::cache::Engine::OpenCache to open up a file.

```{.cxx}
void SomeFunction(cmake::cache::Engine &engine) {
	auto success = engine.OpenCache("./path/to/cache.txt");
	if (!success) {
		// failed to open cache
		return EXIT_FAILURE;
	}
```

The default value of the cache file path is CMakeCache.txt, so we may simply do this:

```{.cxx}
void SomeFunction(cmake::cache::Engine &engine) {
	auto success = engine.OpenCache();
}
```

If the function is successful, then the engine now contains all the variables found in that file.

To get the value of a specific variable, use the @ref cmake::cache::Engine::Find function.

```{.cxx}
void SomeFunction(cmake::cache::Engine &engine) {
	auto variable = engine.Find("CMAKE_CXX_COMPILER");
	if (variable) {
		DoSomething(*variable);
	} else {
		// The variable does not exist.
	}
}
```

The find function will return a variable in an @ref cmake::cache::Optional class.

This is important because we'll have to make sure that it has the variable in it before we use the variable class.

Using the boolean cast operator (as shown above) we can check if the variable was found with an if statement.

Once we have the variable, there are several pieces of information we can get from it.

```{.cxx}
void DoSomething(const cmake::cache::Variable &variable) {

	auto type = variable.GetType();

	const auto &desc = variable.GetDescription();

	const auto &value = variable.GetValue();
}
```

See the @ref cmake::cache::Variable class for details.
