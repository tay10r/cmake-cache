// cmake-cache : A C++ library for reading and writing CMake cache variables.
//
// Copyright (C) 2018  Taylor Holberton
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef CMAKE_CACHE_STREAM_READER_HPP
#define CMAKE_CACHE_STREAM_READER_HPP

#include <cmake/cache/reader.hpp>

#include <iosfwd>

namespace cmake {

namespace cache {

/// This class implements the @ref Reader
/// interface by using a stream from the STL.
class StreamReader : public Reader {
public:
	/// Creates a new stream reader instance.
	/// @param input The input stream to read from.
	/// @returns A pointer to a new stream reader instance.
	static StreamReader *Create(std::istream &input);
	/// Just a stub.
	virtual ~StreamReader() { }
};

} // namespace cache

} // namespace cmake

#endif // CMAKE_CACHE_STREAM_READER_HPP
