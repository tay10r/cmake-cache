// cmake-cache : A C++ library for reading and writing CMake cache variables.
//
// Copyright (C) 2018  Taylor Holberton
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef CMAKE_CACHE_MAP_WRITER_HPP
#define CMAKE_CACHE_MAP_WRITER_HPP

#include <iosfwd>

namespace cmake {

namespace cache {

class Map;

/// This is the interface for the map writer class.
/// This is used to save map contents back to a file.
class MapWriter {
public:
	/// Creates a new map writer instance.
	/// @param output The output stream to write the map to.
	/// @returns A pointer to a new map writer instance.
	static MapWriter *Create(std::ostream &output);
	/// Just a stub.
	virtual ~MapWriter() { }
	/// Writes a map to the output stream that
	/// the writer was assigned to use.
	/// @param map The map to write.
	virtual void Write(const Map &map) = 0;
};

} // namespace cache

} // namespace cmake

#endif // CMAKE_CACHE_MAP_WRITER_HPP
