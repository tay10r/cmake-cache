// cmake-cache : A C++ library for reading and writing CMake cache variables.
//
// Copyright (C) 2018  Taylor Holberton
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef CMAKE_CACHE_TOKEN_CONSUMER_HPP
#define CMAKE_CACHE_TOKEN_CONSUMER_HPP

namespace cmake {

namespace cache {

class Token;

/// This is the interface of any
/// class that receives tokens generated
/// by the lexer.
class TokenConsumer {
public:
	/// Just a stub.
	virtual ~TokenConsumer() { }
	/// This is called by the lexer
	/// when ever it finishes scanning
	/// a token from the source code.
	/// @param token The token that the lexer found.
	virtual void Consume(const Token &token) = 0;
};

} // namespace cache

} // namespace cmake

#endif // CMAKE_CACHE_TOKEN_CONSUMER_HPP
