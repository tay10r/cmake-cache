// cmake-cache : A C++ library for reading and writing CMake cache variables.
//
// Copyright (C) 2018  Taylor Holberton
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef CMAKE_CACHE_ERROR_PRINTER_HPP
#define CMAKE_CACHE_ERROR_PRINTER_HPP

#include <cmake/cache/error-handler.hpp>

#include <iosfwd>

namespace cmake {

namespace cache {

/// This class is used to print errors to a stream.
class ErrorPrinter : public ErrorHandler {
public:
	/// Creates a new error printer instance.
	/// @param output The stream to print the errors to.
	/// @returns A pointer to a new error printer instance.
	/// The caller is responsible for deleting this instance.
	static ErrorPrinter *Create(std::ostream &output);
	/// Just a stub.
	virtual ~ErrorPrinter() { }
};

} // namespace cache

} // namespace cmake

#endif // CMAKE_CACHE_ERROR_PRINTER_HPP
