// cmake-cache : A C++ library for reading and writing CMake cache variables.
//
// Copyright (C) 2018  Taylor Holberton
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef CMAKE_CACHE_NODE_VISITOR_HPP
#define CMAKE_CACHE_NODE_VISITOR_HPP

namespace cmake {

namespace cache {

class UnexpectedToken;
class UnusedToken;
class VariableDecl;

/// Used for visiting the derived node classes.
class NodeVisitor {
public:
	/// Just a stub.
	virtual ~NodeVisitor() { }
	/// Visits an unexpected token.
	virtual void Visit(const UnexpectedToken &) = 0;
	/// Visits an unused token.
	virtual void Visit(const UnusedToken &) = 0;
	/// Visits an variable declaration.
	virtual void Visit(const VariableDecl &) = 0;
};

} // namespace cache

} // namespace cmake

#endif // CMAKE_CACHE_NODE_VISITOR_HPP
