// cmake-cache : A C++ library for reading and writing CMake cache variables.
//
// Copyright (C) 2018  Taylor Holberton
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef CMAKE_CACHE_PARSER_HPP
#define CMAKE_CACHE_PARSER_HPP

namespace cmake {

namespace cache {

class Lexer;
class NodeConsumer;

/// The interface for the parser class.
/// Used for parsing cache files.
class Parser {
public:
	/// Creates a parser instance.
	/// @returns A pointer to a new parser instance.
	static Parser *Create();
	/// Just a stub.
	virtual ~Parser() { }
	/// Attempts to parse a single node.
	/// @returns True if a node was parsed,
	/// false if a node was not parsed. If
	/// a node was not able to be parsed, that
	/// is most likely because the end of file
	/// was reached.
	virtual bool Parse() = 0;
	/// Assigns the lexer for the parser to use.
	/// @param lexer The lexer to assign to the parser.
	virtual void SetLexer(Lexer &lexer) = 0;
	/// Assigns the node consumer for the parser to
	/// pass the matched nodes to.
	/// @param consumer The node consumer to assign.
	virtual void SetNodeConsumer(NodeConsumer &consumer) = 0;
};

} // namespace cache

} // namespace cmake

#endif // CMAKE_CACHE_PARSER_HPP
