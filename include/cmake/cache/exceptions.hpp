// cmake-cache : A C++ library for reading and writing CMake cache variables.
//
// Copyright (C) 2018  Taylor Holberton
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef CMAKE_CACHE_EXCEPTIONS_HPP
#define CMAKE_CACHE_EXCEPTIONS_HPP

#include <stdexcept>

namespace cmake {

namespace cache {

/// This is the base class of all exceptions
/// that may occur within this library.
class Exception : public std::exception {
public:
	/// Just a stub.
	virtual ~Exception() { }
};

/// This exception is thrown when an optional
/// container is accessed with no value.
class BadOptionalAccess final : public Exception {
public:
	/// Just a stub.
	~BadOptionalAccess() {

	}
	/// Accesses a description of this exception.
	/// @returns A description of this exception.
	const char *what() const noexcept override {
		return "bad optional access";
	}
};

} // namespace cache

} // namespace cmake

#endif // CMAKE_CACHE_EXCEPTIONS_HPP
