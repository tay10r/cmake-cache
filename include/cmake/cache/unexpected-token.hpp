// cmake-cache : A C++ library for reading and writing CMake cache variables.
//
// Copyright (C) 2018  Taylor Holberton
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef CMAKE_CACHE_UNEXPECTED_TOKEN_HPP
#define CMAKE_CACHE_UNEXPECTED_TOKEN_HPP

#include <cmake/cache/node.hpp>

namespace cmake {

namespace cache {

class Token;

/// Represents a token that was found
/// by the parser where it wasn't expected to be.
class UnexpectedToken : public Node {
public:
	/// Just a stub.
	virtual ~UnexpectedToken() { }
	/// Accesses the token that was unexpected.
	/// @returns A constant reference to the token
	/// that was unexpected.
	virtual const Token &GetToken() const noexcept = 0;
};

} // namespace cache

} // namespace cmake

#endif // CMAKE_CACHE_UNEXPECTED_TOKEN_HPP
