// cmake-cache : A C++ library for reading and writing CMake cache variables.
//
// Copyright (C) 2018  Taylor Holberton
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef CMAKE_CACHE_TOKEN_ITERATOR_HPP
#define CMAKE_CACHE_TOKEN_ITERATOR_HPP

namespace cmake {

namespace cache {

class Token;

/// This is the interface of the token iterator class.
/// It provides a way of iterating through a container
/// of tokens, without having to access the container implementation.
class TokenIterator {
public:
	/// Just a stub.
	virtual ~TokenIterator() { }
	/// Indicates whether or not the token iterator
	/// has reached the end of the container. After
	/// the iterator has reached the end, it should
	/// no longer be used to access tokens.
	/// @returns True if the iterator has reached
	/// the end, false otherwise.
	virtual bool AtEnd() const noexcept = 0;
	/// Accesses the token that the iterator is currently
	/// pointing at. This should not be called after the
	/// iterator has reached the end of the container.
	/// @returns A constant reference to the token that
	/// the iterator is currently pointing at.
	virtual const Token &GetCurrent() const noexcept = 0;
	/// Goes to the next token in the container.
	/// If the iterator is already at the end of
	/// the container, then this function has no effect.
	virtual void Next() noexcept = 0;
};

} // namespace cache

} // namespace cmake

#endif // CMAKE_CACHE_TOKEN_ITERATOR_HPP
