// cmake-cache : A C++ library for reading and writing CMake cache variables.
//
// Copyright (C) 2018  Taylor Holberton
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef CMAKE_CACHE_READER_HPP
#define CMAKE_CACHE_READER_HPP

#include <cstddef>

namespace cmake {

namespace cache {

/// Used for reading source code from
/// an arbitrary source.
class Reader {
public:
	/// Just a stub.
	virtual ~Reader() { }
	/// Reads data from the source.
	/// @param data A pointer to a character
	/// array to put the data into.
	/// @param size The number of characters to read.
	/// @returns The number of characters that were read.
	virtual size_t Read(char *data, size_t size) = 0;
};

} // namespace cache

} // namespace cmake

#endif // CMAKE_CACHE_READER_HPP
