// cmake-cache : A C++ library for reading and writing CMake cache variables.
//
// Copyright (C) 2018  Taylor Holberton
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef CMAKE_CACHE_VERSION_HPP
#define CMAKE_CACHE_VERSION_HPP

/// Contains the major version of the library.
#define CMAKE_CACHE_VERSION_MAJOR 0x00

/// Contains the minor version of the library.
#define CMAKE_CACHE_VERSION_MINOR 0x01

/// Contains the patch version of the library.
#define CMAKE_CACHE_VERSION_PATCH 0x02

/// Contains the entire version number.
#define CMAKE_CACHE_VERSION 0x000102

/// Contains a human readable version string.
#define CMAKE_CACHE_VERSION_STRING "0.1.2"

#endif // CMAKE_CACHE_VERSION_HPP
