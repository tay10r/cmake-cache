// cmake-cache : A C++ library for reading and writing CMake cache variables.
//
// Copyright (C) 2018  Taylor Holberton
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef CMAKE_CACHE_MAP_HPP
#define CMAKE_CACHE_MAP_HPP

#include <cmake/cache/optional.hpp>

namespace cmake {

namespace cache {

class Variable;
class VariableIterator;

/// Represents a variable key-value map.
class Map {
public:
	/// Creates a map instance.
	/// @returns A new map instance.
	static Map *Create();
	/// Just a stub.
	virtual ~Map() { }
	/// Creates a variable iterator.
	/// The variable iterator can be used to
	/// iterate the variables contained within the map.
	/// @returns A pointer to a new variable iterator instance.
	/// The caller is responsible for deleting this instance.
	virtual VariableIterator *CreateIterator() const = 0;
	/// Defines a variable.
	/// If the variable of the given name already
	/// exists, then it is overwritten after calling this function.
	/// @param name The name to give the variable.
	/// @param variable The variable type and value to define.
	virtual void Define(const char *name, Variable *variable) = 0;
	/// Locates a variable by name.
	/// @param name The name of the variable to locate.
	/// @returns The variable is returned, if it is found.
	/// Otherwise, an empty optional container is returned.
	virtual Optional<Variable> Find(const char *name) const = 0;
};

} // namespace cache

} // namespace cmake

#endif // CMAKE_CACHE_MAP_HPP
