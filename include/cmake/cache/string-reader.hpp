// cmake-cache : A C++ library for reading and writing CMake cache variables.
//
// Copyright (C) 2018  Taylor Holberton
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef CMAKE_CACHE_STRING_READER_HPP
#define CMAKE_CACHE_STRING_READER_HPP

#include <cmake/cache/reader.hpp>

#include <cstddef>

namespace cmake {

namespace cache {

/// This class implements the @ref Reader
/// interface by using a stream from the STL.
class StringReader final : public Reader {
	/// The string data being read.
	const char *data;
	/// The number of characters being read.
	size_t size;
	/// The position of the reader within the string.
	size_t position;
public:
	/// Constructs the string reader.
	/// @param data_ The data to read.
	StringReader(const char *data_) noexcept;
	/// Constructs the string reader.
	/// @param data_ The data to read.
	/// @param size_ The number of characters to read.
	StringReader(const char *data_, size_t size_) noexcept;
	/// Just a stub.
	~StringReader();
	/// Reads data from the string.
	/// @param data The buffer to put the data into.
	/// @param size The number of characters to read.
	/// @returns The number of characters that were read.
	size_t Read(char *data, size_t size) override;
};

} // namespace cache

} // namespace cmake

#endif // CMAKE_CACHE_STRING_READER_HPP
