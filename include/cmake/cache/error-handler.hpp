// cmake-cache : A C++ library for reading and writing CMake cache variables.
//
// Copyright (C) 2018  Taylor Holberton
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef CMAKE_CACHE_ERROR_HANDLER_HPP
#define CMAKE_CACHE_ERROR_HANDLER_HPP

namespace cmake {

namespace cache {

class Error;

/// This is the interface for any class
/// that handles errors found within the source code.
class ErrorHandler {
public:
	/// Just a stub.
	virtual ~ErrorHandler() { }
	/// Handles an error detected by the parser.
	/// @param error The error detected by the parser.
	virtual void Handle(const Error &error) = 0;
	/// Updates the path of the source file that is being parsed.
	/// @param path The path of the source file being parsed.
	virtual void UpdateSourcePath(const char *path) = 0;
};

} // namespace cache

} // namespace cmake

#endif // CMAKE_CACHE_ERROR_HANDLER_HPP
