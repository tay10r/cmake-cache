// cmake-cache : A C++ library for reading and writing CMake cache variables.
//
// Copyright (C) 2018  Taylor Holberton
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef CMAKE_CACHE_TOKEN_HPP
#define CMAKE_CACHE_TOKEN_HPP

#include <string>

namespace cmake {

namespace cache {

/// This is the interface for a source code token.
class Token {
public:
	/// Enumerates the several possible
	/// token types.
	enum class Type {
		/// This token is used for special
		/// cases in which a token is not assigned
		/// a value.
		None,
		/// The token is a single colon.
		Colon,
		/// The token is a comment line.
		Comment,
		/// The token is a variable description line.
		DescriptionLine,
		/// The token is a single '=' character.
		EqualSign,
		/// The token is an identifier.
		Identifier,
		/// The token is a newline sequence.
		Newline,
		/// The token is a space or tab.
		Space,
		/// The token is any other character
		/// not covered by the other token types.
		Other
	};
	/// Just a stub.
	virtual ~Token() { }
	/// Copies this token.
	/// @returns A new token instance,
	/// containing a copy of the data
	/// found in this token.
	virtual Token *Copy() const = 0;
	/// Accesses the token string data.
	/// @returns A reference to the string data.
	virtual const std::string &GetData() const noexcept = 0;
	/// Accesses the token type.
	/// @returns The token type.
	virtual Type GetType() const noexcept = 0;
	/// Accesses the line number that the token appears on.
	/// @returns The line number that the token appears on.
	virtual size_t GetLine() const noexcept = 0;
	/// Accesses the column number that the token appears on.
	/// @returns The column number that the token appears on.
	virtual size_t GetColumn() const noexcept = 0;
};

} // namespace cache

} // namespace cmake

#endif // CMAKE_CACHE_TOKEN_HPP
