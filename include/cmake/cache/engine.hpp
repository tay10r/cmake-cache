// cmake-cache : A C++ library for reading and writing CMake cache variables.
//
// Copyright (C) 2018  Taylor Holberton
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef CMAKE_CACHE_ENGINE_HPP
#define CMAKE_CACHE_ENGINE_HPP

#include <cmake/cache/optional.hpp>
#include <cmake/cache/variable.hpp>

#include <iosfwd>

namespace cmake {

namespace cache {

class Variable;

/// This is an interface to the library.
/// It is used as a facade to simplify the
/// various mechanisms in the library into
/// a single interface.
class Engine {
public:
	/// Creates a new engine instance.
	/// @returns A pointer to the new engine instance.
	static Engine *Create();
	/// Just a stub.
	virtual ~Engine() { }
	/// Defines a new variable.
	/// @param name The name of the variable to define.
	/// @param type The type to give the variable.
	/// @param value The value to give the variable.
	virtual void Define(const std::string &name, Variable::Type type, const std::string &value) = 0;
	/// Defines a new string variable.
	/// @param name The name of the variable to define.
	/// @param value The value to give the variable.
	virtual void DefineString(const std::string &name, const std::string &value) = 0;
	/// Defines a new boolean variable.
	/// @param name The name of the variable to define.
	/// @param value The value to give the variable.
	virtual void DefineBool(const std::string &name, bool value) = 0;
	/// Reads a cache from a string.
	/// @param source The string to read
	/// the cache source code from.
	/// @param filename The file name of the code being parse.
	/// This is used for error information.
	virtual void ReadCache(const std::string &source, const std::string &filename = "(unknown origin)") = 0;
	/// Reads a cache from an input stream.
	/// @param input The stream to read the
	/// cache source code from.
	/// @param filename The file name of the code being parse.
	/// This is used for error information.
	virtual void ReadCache(std::istream &input, const std::string &filename = "(unknown origin)") = 0;
	/// Opens up a cache file.
	/// @param path The path of the file to open.
	/// The default value is "CMakeCache.txt".
	/// @returns True if the file was opened successfully,
	/// false if the file could not be opened.
	virtual bool OpenCache(const std::string &path = "CMakeCache.txt") = 0;
	/// Writes the cache variable map to a file.
	/// The variables are sorted alphabetically.
	/// @param path The path of the file to save
	/// the cache variables to. The default value
	/// is "CMakeCache.txt".
	/// @returns True if the file was written to
	/// successfully, false if the file was not.
	virtual bool SaveCache(const std::string &path = "CMakeCache.txt") = 0;
	/// Locates a variable in the cache.
	/// @param name The name of the variable to find.
	/// @returns An optional variable container. If
	/// the variable was not found, then the optional
	/// container will not contain a value. If the variable
	/// was found, then the optional container will contain
	/// the variable that was found.
	virtual Optional<Variable> Find(const std::string &name) = 0;
	/// Writes the cache to a stream.
	/// @param output The stream to write the source code to.
	virtual void WriteCache(std::ostream &output) = 0;
};

} // namespace cache

} // namespace cmake

#endif // CMAKE_CACHE_ENGINE_HPP
