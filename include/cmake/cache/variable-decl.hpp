// cmake-cache : A C++ library for reading and writing CMake cache variables.
//
// Copyright (C) 2018  Taylor Holberton
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef CMAKE_CACHE_VARIABLE_DECL_HPP
#define CMAKE_CACHE_VARIABLE_DECL_HPP

#include <cmake/cache/node.hpp>

namespace cmake {

namespace cache {

class Token;
class TokenList;

/// This is the interface for
/// variable declaration nodes.
class VariableDecl : public Node {
public:
	/// Just a stub.
	virtual ~VariableDecl() { }
	/// Accesses the list of tokens that make up the variable description.
	/// @returns A reference to the list of tokens containing the variable description.
	virtual const TokenList &GetDescriptionLineList() const noexcept = 0;
	/// Accesses the variable name being declared.
	/// @returns The token containing the variable name.
	virtual const Token &GetVariableName() const noexcept = 0;
	/// Accesses the colon that follows the variable
	/// name and precedes the variable type.
	/// @returns The colon that follows the variable name.
	virtual const Token &GetColon() const noexcept = 0;
	/// Accesses the token containing the name of the variable type.
	/// @returns The token containing the name of the variable type.
	virtual const Token &GetTypeName() const noexcept = 0;
	/// Accesses the equal sign that follows the variable type
	/// and comes before the value list.
	/// @returns A token containing the equals sign.
	virtual const Token &GetEqualSign() const noexcept = 0;
	/// Accesses the list of tokens that make up the value being assigned.
	/// @returns The list of tokens that make up the value.
	virtual const TokenList &GetValueList() const noexcept = 0;
};

} // namespace cache

} // namespace cmake

#endif // CMAKE_CACHE_VARIABLE_DECL_HPP
