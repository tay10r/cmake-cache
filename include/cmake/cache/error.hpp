// cmake-cache : A C++ library for reading and writing CMake cache variables.
//
// Copyright (C) 2018  Taylor Holberton
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef CMAKE_CACHE_ERROR_HPP
#define CMAKE_CACHE_ERROR_HPP

#include <string>

#include <cstddef>

namespace cmake {

namespace cache {

/// The interface for an error instance.
class Error {
public:
	/// Just a stub.
	virtual ~Error() { }
	/// Accesses a description of the error.
	/// @returns A description of the error.
	virtual const std::string &GetDescription() const noexcept = 0;
	/// Accesses the line number that the error occurred at.
	/// @returns The line number that the error occurred at.
	virtual size_t GetLine() const noexcept = 0;
	/// Accesses the column number that the error occurred at.
	/// @returns The column number that the error occurred at.
	virtual size_t GetColumn() const noexcept = 0;
};

} // namespace cache

} // namespace cmake

#endif // CMAKE_CACHE_ERROR_HPP
