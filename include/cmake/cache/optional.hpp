// cmake-cache : A C++ library for reading and writing CMake cache variables.
//
// Copyright (C) 2018  Taylor Holberton
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef CMAKE_CACHE_OPTIONAL_HPP
#define CMAKE_CACHE_OPTIONAL_HPP

#include <cmake/cache/exceptions.hpp>

namespace cmake {

namespace cache {

/// This container is used for values
/// that may optionally be present.
/// This serves as an alternative to
/// raw pointers.
/// @tparam Value The type of the value
/// contained within the class.
template <typename Value>
class Optional final {
	/// A pointer to the optional value.
	Value *valuePointer;
public:
	/// Constructs the optional container
	/// without a value.
	Optional() noexcept : valuePointer(nullptr) {

	}
	/// Constructs the optional container.
	/// @param valuePointer_ The pointer to
	/// the value being contained. A null pointer
	/// is considered to be a "no-value".
	Optional(Value *valuePointer_) noexcept : valuePointer(valuePointer_) {

	}
	/// Accesses the value in the optional container.
	/// If the container does not have a value, then
	/// an exception will be thrown.
	/// @returns A constant reference to the
	/// value being returned.
	const Value *operator -> () const {
		return valuePointer;
	}
	/// Indicates whether or not the container
	/// has a valid value in it. This can be useful
	/// for preventing exceptions from being thrown
	/// when accessing the value.
	/// @returns True if the container has a value,
	/// false otherwise.
	operator bool () const noexcept {
		return valuePointer != nullptr;
	}
};

} // namespace cache

} // namespace cmake

#endif // CMAKE_CACHE_OPTIONAL_HPP
