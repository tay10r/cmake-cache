// cmake-cache : A C++ library for reading and writing CMake cache variables.
//
// Copyright (C) 2018  Taylor Holberton
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef CMAKE_CACHE_MAP_BUILDER_HPP
#define CMAKE_CACHE_MAP_BUILDER_HPP

#include <cmake/cache/node-consumer.hpp>

namespace cmake {

namespace cache {

class ErrorHandler;
class Map;

/// This is the interface to the map builder.
/// The map builder is used to construct maps
/// from the parser.
class MapBuilder : public NodeConsumer {
public:
	/// Creates a new map builder instance.
	/// @param map A reference to the map being built.
	/// @returns A pointer to a new map builder instance.
	static MapBuilder *Create(Map &map);
	/// Just a stub.
	virtual ~MapBuilder() { }
	/// Assigns the error handler to pass error messages to.
	/// @param errorHandler The error handler to pass to the map builder.
	virtual void SetErrorHandler(ErrorHandler &errorHandler) = 0;
};

} // namespace cache

} // namespace cmake

#endif // CMAKE_CACHE_MAP_BUILDER_HPP
