// cmake-cache : A C++ library for reading and writing CMake cache variables.
//
// Copyright (C) 2018  Taylor Holberton
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef CMAKE_CACHE_LEXER_HPP
#define CMAKE_CACHE_LEXER_HPP

namespace cmake {

namespace cache {

class Reader;
class TokenConsumer;

/// Used for scanning tokens of a CMake cache file.
class Lexer {
public:
	/// Creates a new lexer instance.
	/// @returns A pointer to a new lexer instance.
	static Lexer *Create();
	/// Just a stub.
	virtual ~Lexer() { }
	/// Scans the input for tokens.
	/// @returns True if a token was found,
	/// false if it was not. False is returned
	/// when the end of file is reached as well.
	virtual bool Scan() = 0;
	/// Assigns the token consumer.
	/// @param consumer The token consumer to assign.
	virtual void SetTokenConsumer(TokenConsumer &consumer) = 0;
	/// Assigns the reader of the cache file.
	/// @param reader The reader of the cache file.
	virtual void SetReader(Reader &reader) = 0;
};

} // namespace cache

} // namespace cmake

#endif // CMAKE_CACHE_LEXER_HPP
