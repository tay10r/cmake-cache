// cmake-cache : A C++ library for reading and writing CMake cache variables.
//
// Copyright (C) 2018  Taylor Holberton
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef CMAKE_CACHE_VARIABLE_ITERATOR_HPP
#define CMAKE_CACHE_VARIABLE_ITERATOR_HPP

#include <string>

namespace cmake {

namespace cache {

/// This is the interface for the variable iterator.
/// The variable iterator is used for iterating the
/// entries within the variable map, without exposing
/// the map implementation.
class VariableIterator {
public:
	/// Just a stub.
	virtual ~VariableIterator() { }
	/// Indicates whether or not the iterator
	/// has reached the end of the container.
	/// When the iterator reaches the end of the
	/// container, the iterator can no longer be
	/// dereferenced.
	/// @returns True if the end of the container
	/// was reached, false if it was not.
	virtual bool AtEnd() const noexcept = 0;
	/// Gets the variable that the iterator is
	/// currently pointing at. This should not be
	/// called when the iterator has reached the
	/// end of the container.
	/// @returns A constant reference to the variable
	/// that the iteratir is currently pointing at.
	/// If this this is called at the end of the container,
	/// an empty variable with an "unknown" type is returned.
	virtual const Variable &GetCurrent() const noexcept = 0;
	/// Gets the name of the variable the iterator
	/// is currently sitting at. This should not be used
	/// when the iterator reaches the end of the container.
	/// @returns A constant reference to the current variable
	/// name. If this function is called at the end of the
	/// container, then an empty string is returned.
	virtual const std::string &GetCurrentName() const noexcept = 0;
	/// Goes to the next variable in the container.
	/// If this function is called at the end of the
	/// container, then it has no effect.
	virtual void Next() noexcept = 0;
};

} // namespace cache

} // namespace cmake

#endif // CMAKE_CACHE_VARIABLE_ITERATOR_HPP
