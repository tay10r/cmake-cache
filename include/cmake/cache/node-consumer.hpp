// cmake-cache : A C++ library for reading and writing CMake cache variables.
//
// Copyright (C) 2018  Taylor Holberton
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef CMAKE_CACHE_NODE_CONSUMER_HPP
#define CMAKE_CACHE_NODE_CONSUMER_HPP

namespace cmake {

namespace cache {

class Node;

/// This is the interface for a class
/// that receives nodes from the parser.
class NodeConsumer {
public:
	/// Just a stub.
	virtual ~NodeConsumer() { }
	/// Called by the parser when a node is found.
	/// @param node The node found by the parser.
	virtual void Consume(const Node &node) = 0;
};

} // namespace cache

} // namespace cmake

#endif // CMAKE_CACHE_NODE_CONSUMER_HPP
