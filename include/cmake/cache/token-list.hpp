// cmake-cache : A C++ library for reading and writing CMake cache variables.
//
// Copyright (C) 2018  Taylor Holberton
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef CMAKE_CACHE_TOKEN_LIST_HPP
#define CMAKE_CACHE_TOKEN_LIST_HPP

namespace cmake {

namespace cache {

class TokenIterator;

/// This is the interface for the token list class.
/// It can be used to iterate through the tokens of
/// a value list or a description line list.
class TokenList {
public:
	/// Just a stub.
	virtual ~TokenList() { }
	/// Indicates whether or not the token list is empty.
	/// @returns True if it is empty, false if it is not.
	virtual bool Empty() const noexcept = 0;
	/// Creates a token iterator.
	/// @returns A pointer to a new token iterator instance.
	virtual TokenIterator *CreateIterator() const = 0;
};

} // namespace cache

} // namespace cmake

#endif // CMAKE_CACHE_TOKEN_LIST_HPP
