mkdir build

cd build

if "%platform%"=="x86" (
	set cmake_target_arch=
)

if "%platform%"=="x64" (
	set cmake_target_arch= Win64
)

if "%APPVEYOR_BUILD_WORKER_IMAGE%"=="Visual Studio 2017" (
	cmake .. -G "Visual Studio 15 2017%cmake_target_arch%"
)

if "%APPVEYOR_BUILD_WORKER_IMAGE%"=="Visual Studio 2015" (
	cmake .. -G "Visual Studio 14 2015%cmake_target_arch%"
)

if "%APPVEYOR_BUILD_WORKER_IMAGE%"=="Visual Studio 2013" (
	cmake .. -G "Visual Studio 12 2013%cmake_target_arch%"
)

cd ..
