#!/bin/sh

set -e
set -u

mkdir build && cd build

cmake --version

cmake ..

cmake --build .

ctest --verbose

cpack -G DEB
cpack -G TGZ
cpack -G RPM
