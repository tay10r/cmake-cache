cmake-cache
===========

[![Build Status](https://travis-ci.org/tay10r/cmake-cache.svg?branch=master)](https://travis-ci.org/tay10r/cmake-cache)
[![Build status](https://ci.appveyor.com/api/projects/status/45wiylojljmjhln0?svg=true)](https://ci.appveyor.com/project/tay10r/cmake-cache)

This library is intended for reading and writing CMake cache files.

It can also be used for syntax highlighting.

### Usage

The easiest way of using the API is by use of the `Engine` class.

Here's an example.

```c++
void RunExampleFunction() {

	using namespace cmake::cache;

	auto engine = Engine::Create();

	engine->OpenCache();

	auto var = engine->Find("CMAKE_CXX_COMPILER");

	if (var) {
		DoSomethingWithVariable(*var);
	}

	engine->SaveCache();
}
```

You can also take a look at the `get-value-of` example program.

Running the example just involves specifying the names of the variables to get the value of.

```
./get-value-of CMAKE_CXX_COMPILER
```

When the project is installed on the system, you can use CMake to find it.

```cmake
cmake_minimum_required(VERSION 3.0)

find_package(CMakeCache)

add_executable("example" "example.cpp")

target_link_libraries("example" CMakeCache::cmake-cache)
```

### Building

To build this project, you'll need CMake installed.

On Ubuntu, you can use this command:

```bash
sudo apt install cmake
# or sudo apt-get install cmake
```

Once CMake is installed, you can do this to build the project.

```
mkdir build
cd build
cmake ..
cmake --build .
```

To run the tests, use the `ctest` command.

```
ctest --verbose
```
