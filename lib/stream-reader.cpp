// cmake-cache : A C++ library for reading and writing CMake cache variables.
//
// Copyright (C) 2018  Taylor Holberton
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include <cmake/cache/stream-reader.hpp>

#include <istream>

namespace cmake {

namespace cache {

namespace {

namespace impl {

class StreamReader final : public cache::StreamReader {
	std::istream &input;
public:
	StreamReader(std::istream &input_) noexcept : input(input_) {

	}
	~StreamReader() {

	}
	size_t Read(char *data, size_t size) override {

		input.read(data, size);

		return (size_t) input.gcount();
	}
};

} // namespace impl

} // namespace

StreamReader *StreamReader::Create(std::istream &input) {
	return new impl::StreamReader(input);
}

} // namespace cache

} // namespace cmake
