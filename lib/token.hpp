// cmake-cache : A C++ library for reading and writing CMake cache variables.
//
// Copyright (C) 2018  Taylor Holberton
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef CMAKE_CACHE_IMPL_TOKEN_HPP
#define CMAKE_CACHE_IMPL_TOKEN_HPP

#include <cmake/cache/token.hpp>

namespace cmake {

namespace cache {

namespace {

namespace impl {

class Token final : public cache::Token {
	Token::Type type;
	std::string data;
	size_t line;
	size_t column;
public:
	Token() {
		line = 1;
		column = 1;
	}
	Token(const char *data_, Token::Type type_) {
		type = type_;
		if (data_ != nullptr) {
			data = data_;
		}
		line = 1;
		column = 1;
	}
	~Token() {

	}
	cache::Token *Copy() const override {
		return new Token(*this);
	}
	const std::string &GetData() const noexcept override {
		return data;
	}
	Token::Type GetType() const noexcept override {
		return type;
	}
	size_t GetLine() const noexcept override {
		return line;
	}
	size_t GetColumn() const noexcept override {
		return column;
	}
	Token &operator = (const cache::Token &other) {
		type = other.GetType();
		data = other.GetData();
		line = other.GetLine();
		column = other.GetColumn();
		return *this;
	}
protected:
	Token(const Token &other) {
		type = other.type;
		data = other.data;
		line = other.line;
		column = other.column;
	}
};

} // namespace impl

} // namespace

} // namespace cache

} // namespace cmake

#endif // CMAKE_CACHE_IMPL_TOKEN_HPP
