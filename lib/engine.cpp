// cmake-cache : A C++ library for reading and writing CMake cache variables.
//
// Copyright (C) 2018  Taylor Holberton
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include <cmake/cache/engine.hpp>

#include <cmake/cache/error-printer.hpp>
#include <cmake/cache/lexer.hpp>
#include <cmake/cache/map.hpp>
#include <cmake/cache/map-builder.hpp>
#include <cmake/cache/map-writer.hpp>
#include <cmake/cache/parser.hpp>
#include <cmake/cache/stream-reader.hpp>
#include <cmake/cache/string-reader.hpp>

#include <fstream>
#include <iostream>
#include <memory>

namespace cmake {

namespace cache {

namespace {

namespace impl {

class Engine;

class Variable final : public cache::Variable {
	friend Engine;
	std::string description;
	std::string value;
	Variable::Type type;
public:
	Variable() {
		type = Variable::Type::Unknown;
	}
	~Variable() {

	}
	const std::string &GetDescription() const noexcept override {
		return description;
	}
	Variable::Type GetType() const noexcept override {
		return type;
	}
	const std::string &GetValue() const noexcept override {
		return value;
	}
};

class Engine final : public cache::Engine {
	std::unique_ptr<Lexer> lexer;
	std::unique_ptr<Parser> parser;
	std::unique_ptr<Map> map;
	std::unique_ptr<MapBuilder> mapBuilder;
	std::unique_ptr<ErrorHandler> errorHandler;
public:
	Engine() {

		errorHandler = std::unique_ptr<ErrorHandler>(ErrorPrinter::Create(std::cerr));

		map = std::unique_ptr<Map>(Map::Create());

		mapBuilder = std::unique_ptr<MapBuilder>(MapBuilder::Create(*map));
		mapBuilder->SetErrorHandler(*errorHandler);

		lexer = std::unique_ptr<Lexer>(Lexer::Create());

		parser = std::unique_ptr<Parser>(Parser::Create());
		parser->SetLexer(*lexer);
		parser->SetNodeConsumer(*mapBuilder);
	}
	~Engine() {

	}
	void Define(const std::string &name, Variable::Type type, const std::string &value) override {

		std::unique_ptr<Variable> variable(new Variable);

		variable->type = type;
		variable->value = value;

		map->Define(name.c_str(), variable.release());
	}
	void DefineBool(const std::string &name, bool value) override {

		std::unique_ptr<Variable> variable(new Variable);

		variable->type = Variable::Type::Bool;

		if (value) {
			variable->value = "ON";
		} else {
			variable->value = "OFF";
		}

		map->Define(name.c_str(), variable.release());
	}
	void DefineString(const std::string &name, const std::string &value) override {

		std::unique_ptr<Variable> variable(new Variable);

		variable->type = Variable::Type::String;

		variable->value = value;

		map->Define(name.c_str(), variable.release());
	}
	void ReadCache(const std::string &source, const std::string &filename) override {

		StringReader reader(source.c_str(), source.size());

		Parse(reader, filename);
	}
	void ReadCache(std::istream &input, const std::string &filename) override {

		std::unique_ptr<StreamReader> reader(StreamReader::Create(input));

		Parse(*reader, filename);
	}
	bool OpenCache(const std::string &path) override {

		std::ifstream file(path);
		if (!file.good()) {
			return false;
		}

		ReadCache(file, path);

		return true;
	}
	bool SaveCache(const std::string &) override {
		return false;
	}
	void WriteCache(std::ostream &output) override {

		std::unique_ptr<MapWriter> mapWriter(MapWriter::Create(output));

		mapWriter->Write(*map);
	}
	Optional<cache::Variable> Find(const std::string &name) override {
		return map->Find(name.c_str());
	}
protected:
	void Parse(Reader &reader, const std::string &filename) {

		if (errorHandler) {
			errorHandler->UpdateSourcePath(filename.c_str());
		}

		lexer->SetReader(reader);

		Parse();
	}
	void Parse() {
		for (;;) {
			if (!parser->Parse()) {
				break;
			}
		}
	}
};

} // namespace impl

} // namespace

Engine *Engine::Create() {
	return new impl::Engine;
}

} // namespace cache

} // namespace cmake
