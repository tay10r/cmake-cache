// cmake-cache : A C++ library for reading and writing CMake cache variables.
//
// Copyright (C) 2018  Taylor Holberton
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include <cmake/cache/map-writer.hpp>

#include <cmake/cache/map.hpp>
#include <cmake/cache/variable.hpp>
#include <cmake/cache/variable-iterator.hpp>

#include <memory>
#include <ostream>

namespace cmake {

namespace cache {

namespace {

namespace impl {

class MapWriter final : public cache::MapWriter {
	std::ostream &output;
public:
	MapWriter(std::ostream &output_) noexcept : output(output_) {

	}
	~MapWriter() {

	}
	void Write(const Map &map) override {

		output << "########################" << std::endl;
		output << "# EXTERNAL cache entries" << std::endl;
		output << "########################" << std::endl;
		output << std::endl;
		WriteNormalVariables(map);

		output << "########################" << std::endl;
		output << "# INTERNAL cache entries" << std::endl;
		output << "########################" << std::endl;
		output << std::endl;
		WriteInternalVariables(map);
	}
protected:
	void WriteNormalVariables(const Map &map) {

		std::unique_ptr<VariableIterator> iterator(map.CreateIterator());

		while (!iterator->AtEnd()) {

			if (IsInternal(*iterator)) {
				iterator->Next();
				continue;
			}

			Write(*iterator);

			iterator->Next();

			if (!iterator->AtEnd()) {
				output << std::endl;
			}
		}
	}
	void WriteInternalVariables(const Map &map) {

		std::unique_ptr<VariableIterator> iterator(map.CreateIterator());

		while (!iterator->AtEnd()) {

			if (!IsInternal(*iterator)) {
				iterator->Next();
				continue;
			}

			Write(*iterator);

			iterator->Next();
		}
	}
	bool IsInternal(const VariableIterator &iterator) {
		const auto &var = iterator.GetCurrent();
		return var.GetType() == Variable::Type::Internal;
	}
	void Write(const VariableIterator &iterator) {

		output << iterator.GetCurrentName();
		output << ':';

		const auto &var = iterator.GetCurrent();

		switch (var.GetType()) {
		case Variable::Type::Bool:
			output << "BOOL";
			break;
		case Variable::Type::FilePath:
			output << "FILEPATH";
			break;
		case Variable::Type::DirectoryPath:
			output << "PATH";
			break;
		case Variable::Type::Internal:
			output << "INTERNAL";
			break;
		case Variable::Type::Static:
			output << "STATIC";
			break;
		case Variable::Type::Unknown:
		case Variable::Type::String:
			output << "STRING";
			break;
		}

		output << '=';

		const auto &value = var.GetValue();

		for (auto c : value) {
			if (c == '\n') {
				output << "\\n";
			} else {
				output << c;
			}
		}

		output << std::endl;
	}
};

} // namespace impl

} // namespace

MapWriter *MapWriter::Create(std::ostream &output) {
	return new impl::MapWriter(output);
}

} // namespace cache

} // namespace cmake
