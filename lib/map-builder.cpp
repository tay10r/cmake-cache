// cmake-cache : A C++ library for reading and writing CMake cache variables.
//
// Copyright (C) 2018  Taylor Holberton
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include <cmake/cache/map-builder.hpp>

#include <cmake/cache/error.hpp>
#include <cmake/cache/error-handler.hpp>
#include <cmake/cache/map.hpp>
#include <cmake/cache/node-visitor.hpp>
#include <cmake/cache/token.hpp>
#include <cmake/cache/token-iterator.hpp>
#include <cmake/cache/token-list.hpp>
#include <cmake/cache/unexpected-token.hpp>
#include <cmake/cache/variable.hpp>
#include <cmake/cache/variable-decl.hpp>

#include <memory>

namespace cmake {

namespace cache {

namespace {

namespace impl {

class MapBuilder;

class Variable final : public cache::Variable {
	friend MapBuilder;
	Variable::Type type;
	std::string value;
	std::string description;
public:
	~Variable() {

	}
	const std::string &GetDescription() const noexcept override {
		return description;
	}
	const std::string &GetValue() const noexcept override {
		return value;
	}
	Variable::Type GetType() const noexcept override {
		return type;
	}
};

class Error final : public cache::Error {
	friend MapBuilder;
	std::string description;
	size_t line;
	size_t column;
public:
	Error() {
		line = 1;
		column = 1;
	}
	const std::string &GetDescription() const noexcept override {
		return description;
	}
	size_t GetLine() const noexcept override {
		return line;
	}
	size_t GetColumn() const noexcept override {
		return column;
	}
};

class MapBuilder final : public cache::MapBuilder, public cache::NodeVisitor {
	Map &map;
	ErrorHandler *errorHandler;
public:
	MapBuilder(Map &map_) noexcept : map(map_) {
		errorHandler = nullptr;
	}
	~MapBuilder() {

	}
	void SetErrorHandler(ErrorHandler &errorHandler_) override {
		errorHandler = &errorHandler_;
	}
protected:
	void Consume(const Node &node) override {
		node.Accept(*this);
	}
	void Visit(const VariableDecl &decl) override {
		Build(decl);
	}
	void Visit(const UnusedToken &) override {

	}
	void Visit(const UnexpectedToken &unexpectedToken) override {

		if (errorHandler == nullptr) {
			return;
		}

		Error error;

		error.description = "Unexpected token \"";

		const auto &token = unexpectedToken.GetToken();

		const auto &tokenData = token.GetData();

		for (auto c : tokenData) {
			if (c == '"') {
				error.description.push_back('\\');
				error.description.push_back('"');
			} else {
				error.description.push_back(c);
			}
		}

		error.description += "\" encountered.";

		error.line = token.GetLine();

		error.column = token.GetColumn();

		errorHandler->Handle(error);
	}
protected:
	void Build(const VariableDecl &decl) {

		std::unique_ptr<Variable> variable(new Variable);

		Build(*variable, decl);

		const auto &name = decl.GetVariableName().GetData();

		map.Define(name.c_str(), variable.release());
	}
	void Build(Variable &variable, const VariableDecl &decl) {
		BuildDescription(variable, decl);
		BuildType(variable, decl);
		BuildValue(variable, decl);
	}
	void BuildDescription(Variable &variable, const VariableDecl &decl) {

		const auto &descList = decl.GetDescriptionLineList();

		std::unique_ptr<TokenIterator> descIterator(descList.CreateIterator());

		while (!descIterator->AtEnd()) {

			const auto &descData = descIterator->GetCurrent().GetData();

			/// It isn't required to check for the two forward
			/// slashes, this is just to be clear we don't want
			/// the slashes in the actual description content.
			if ((descData[0] == '/')
			 && (descData[1] == '/')) {
				variable.description += &descData[2];
			}

			descIterator->Next();
		}
	}
	void BuildType(Variable &variable, const VariableDecl &decl) {

		const auto &typeName = decl.GetTypeName().GetData();
		if (typeName == "BOOL") {
			variable.type = Variable::Type::Bool;
		} else if (typeName == "STRING") {
			variable.type = Variable::Type::String;
		} else if (typeName == "FILEPATH") {
			variable.type = Variable::Type::FilePath;
		} else if (typeName == "PATH") {
			variable.type = Variable::Type::DirectoryPath;
		} else if (typeName == "INTERNAL") {
			variable.type = Variable::Type::Internal;
		} else if (typeName == "STATIC") {
			variable.type = Variable::Type::Static;
		} else {
			variable.type = Variable::Type::Unknown;
		}
	}
	void BuildValue(Variable &variable, const VariableDecl &decl) {

		const auto &valueList = decl.GetValueList();

		std::unique_ptr<TokenIterator> valueIterator(valueList.CreateIterator());

		while (!valueIterator->AtEnd()) {

			const auto &valueToken = valueIterator->GetCurrent();

			variable.value += valueToken.GetData();

			valueIterator->Next();
		}
	}
};

} // namespace impl

} // namespace

MapBuilder *MapBuilder::Create(Map &map) {
	return new impl::MapBuilder(map);
}

} // namespace cache

} // namespace cmake
