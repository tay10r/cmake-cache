// cmake-cache : A C++ library for reading and writing CMake cache variables.
//
// Copyright (C) 2018  Taylor Holberton
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include <cmake/cache/map.hpp>

#include <cmake/cache/variable.hpp>
#include <cmake/cache/variable-iterator.hpp>

#include <map>
#include <memory>

namespace cmake {

namespace cache {

namespace {

namespace impl {

class NullVariable final : public Variable {
	std::string emptyString;
public:
	~NullVariable() {

	}
	const std::string &GetValue() const noexcept override {
		return emptyString;
	}
	const std::string &GetDescription() const noexcept {
		return emptyString;
	}
	Variable::Type GetType() const noexcept override {
		return Variable::Type::Unknown;
	}
};

class VariableIterator final : public cache::VariableIterator {
	std::map<std::string, std::unique_ptr<Variable>>::const_iterator begin, end, current;
	NullVariable nullVariable;
	std::string emptyString;
public:
	VariableIterator(const std::map<std::string, std::unique_ptr<Variable>> &map) {
		begin = map.begin();
		end = map.end();
		current = begin;
	}
	~VariableIterator() {

	}
	bool AtEnd() const noexcept override {
		return current == end;
	}
	const Variable &GetCurrent() const noexcept override {

		if (AtEnd()) {
			return nullVariable;
		}

		return *current->second;
	}
	const std::string &GetCurrentName() const noexcept override {

		if (AtEnd()) {
			return emptyString;
		}

		return current->first;
	}
	void Next() noexcept override {
		if (current != end) {
			current++;
		}
	}
};

class Map final : public cache::Map {
	std::map<std::string, std::unique_ptr<Variable>> variableMap;
public:
	Map() {

	}
	~Map() {

	}
	cache::VariableIterator *CreateIterator() const override {
		return new VariableIterator(variableMap);
	}
	void Define(const char *name, Variable *variable) override {

		if ((name == nullptr) || (variable == nullptr)) {
			return;
		}

		variableMap[name] = std::unique_ptr<Variable>(variable);
	}
	Optional<Variable> Find(const char *name) const override {

		auto it = variableMap.find(name);
		if (it == variableMap.end()) {
			return Optional<Variable>();
		}

		return Optional<Variable>(it->second.get());
	}
};

} // namespace impl

} // namespace

Map *Map::Create() {
	return new impl::Map;
}

} // namespace cache

} // namespace cmake
