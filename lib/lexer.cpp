// cmake-cache : A C++ library for reading and writing CMake cache variables.
//
// Copyright (C) 2018  Taylor Holberton
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include <cmake/cache/lexer.hpp>

#include <cmake/cache/reader.hpp>
#include <cmake/cache/token.hpp>
#include <cmake/cache/token-consumer.hpp>

namespace cmake {

namespace cache {

namespace {

namespace impl {

class Lexer;

class Token final : public cache::Token {
	friend Lexer;
	Token::Type type;
	std::string data;
	size_t line;
	size_t column;
public:
	Token() {
		line = 1;
		column = 1;
	}
	Token(const Token &other) {
		type = other.type;
		data = other.data;
		line = other.line;
		column = other.column;
	}
	~Token() {

	}
	Token *Copy() const override {
		return new Token(*this);
	}
	const std::string &GetData() const noexcept override {
		return data;
	}
	Token::Type GetType() const noexcept override {
		return type;
	}
	size_t GetLine() const noexcept override {
		return line;
	}
	size_t GetColumn() const noexcept override {
		return column;
	}
};

const unsigned char replacementChar[] = {
	0xef,
	0xbf,
	0xbd
};

void InsertReplacementChar(std::string &str) {
	str.push_back((char) replacementChar[0]);
	str.push_back((char) replacementChar[1]);
	str.push_back((char) replacementChar[2]);
}

bool CalculateUtf8Size(unsigned char c, size_t &size) {

	if (c <= 0x7f) {
		size = 1;
	} else if (c <= 0xdf) {
		size = 2;
	} else if (c <= 0xef) {
		size = 3;
	} else if (c <= 0xf7) {
		size = 4;
	} else {
		return false;
	}

	return true;
}

// std::isalpha and others throws assertion failures
// on MSVC when c is not within a certain range.
// This is defined here because it is a simple enough
// way to avoid whatever immoral things happen inside
// of MSVC's ctype functions.

bool IsAlpha(char c) noexcept {
	if (((c >= 'a') && (c <= 'z'))
	 || ((c >= 'A') && (c <= 'Z'))) {
		return true;
	} else {
		return false;
	}
}

bool IsAlphaNumerical(char c) noexcept {
	if (IsAlpha(c) || ((c >= '0') && (c <= '9'))) {
		return true;
	} else {
		return false;
	}
}

class Lexer final : public cache::Lexer {
	Reader *reader;
	TokenConsumer *consumer;
	bool lookaheadValid;
	char lookahead;
	Token tmpToken;
	size_t line;
	size_t column;
public:
	Lexer() {
		reader = nullptr;
		consumer = nullptr;
		lookahead = 0;
		lookaheadValid = false;
		line = 1;
		column = 1;
	}
	~Lexer() {

	}
	bool Scan() override {

		if (reader == nullptr) {
			return false;
		} else if (consumer == nullptr) {
			return false;
		}

		char firstChar = 0;
		if (!Peek(firstChar)) {
			return false;
		}

		Next();

		if (IsAlpha(firstChar) || (firstChar == '_')) {
			return ScanIdentifier(firstChar);
		} else if ((firstChar == ' ') || (firstChar == '\t')) {
			return ScanSpace(firstChar);
		} else if ((firstChar == '\n') || (firstChar == '\r')) {
			return ScanNewline(firstChar);
		} else if (firstChar == ':') {
			return ProduceSingleCharacterToken(firstChar, Token::Type::Colon);
		} else if (firstChar == '=') {
			return ProduceSingleCharacterToken(firstChar, Token::Type::EqualSign);
		} else if (firstChar == '#') {
			return ScanComment(firstChar);
		} else if (firstChar == '/') {
			return ScanDescriptionLine(firstChar);
		} else {
			return ScanOther(firstChar);
		}

		return false;
	}
	void SetReader(Reader &reader_) override {
		reader = &reader_;
	}
	void SetTokenConsumer(TokenConsumer &consumer_) override {
		consumer = &consumer_;
	}
protected:
	bool ScanDescriptionLine(char firstChar) {

		char secondChar = 0;

		if (!Peek(secondChar) || (secondChar != '/')) {
			ProduceSingleCharacterToken(firstChar, Token::Type::Other);
			return true;
		}

		Next();

		tmpToken.type = Token::Type::DescriptionLine;
		tmpToken.data.clear();
		tmpToken.data.push_back(firstChar);
		tmpToken.data.push_back(secondChar);

		ReadUntilNewline(tmpToken.data);

		Produce();

		return true;
	}
	bool ScanIdentifier(char firstChar) {

		tmpToken.type = Token::Type::Identifier;
		tmpToken.data.clear();
		tmpToken.data.push_back(firstChar);

		char c = 0;

		while (Peek(c)) {

			if (!IsAlphaNumerical(c)
			 && (c != '_')
			 && (c != '-')) {
				break;
			}

			tmpToken.data.push_back(c);

			Next();
		}

		Produce();

		return true;
	}
	bool ScanSpace(char firstChar) {

		tmpToken.type = Token::Type::Space;
		tmpToken.data.clear();
		tmpToken.data.push_back(firstChar);

		char c = 0;

		while (Peek(c)) {

			if ((c != ' ')
			 && (c != '\t')) {
				break;
			}

			tmpToken.data.push_back(c);

			Next();
		}

		Produce();

		return true;
	}
	bool ScanNewline(char firstChar) {

		tmpToken.type = Token::Type::Newline;
		tmpToken.data.clear();
		tmpToken.data.push_back(firstChar);

		if (firstChar == '\r') {
			char lineFeed = 0;
			if (Peek(lineFeed) && (lineFeed == '\n')) {
				tmpToken.data.push_back(lineFeed);
			}
		}

		Produce();

		return true;
	}
	bool ScanComment(char firstChar) {

		tmpToken.type = Token::Type::Comment;
		tmpToken.data.clear();
		tmpToken.data.push_back(firstChar);

		ReadUntilNewline(tmpToken.data);

		Produce();

		return true;
	}
	bool ScanOther(char firstChar) {

		tmpToken.type = Token::Type::Other;
		tmpToken.data.clear();

		size_t decodeSize = 0;

		if (!CalculateUtf8Size(firstChar, decodeSize)) {
			InsertReplacementChar(tmpToken.data);
			Produce();
			return true;
		}

		tmpToken.data.push_back(firstChar);

		char c = 0;

		for (size_t i = 1; (i < decodeSize) && Peek(c); i++) {
			tmpToken.data.push_back(c);
			Next();
		}

		Produce();

		return true;
	}
	void ReadUntilNewline(std::string &data) {

		char possibleNewline = 0;

		while (Peek(possibleNewline)) {

			if ((possibleNewline == '\r')
			 || (possibleNewline == '\n')) {
				break;
			}

			Next();

			size_t decodeSize = 0;

			if (!CalculateUtf8Size(possibleNewline, decodeSize)) {
				InsertReplacementChar(data);
				continue;
			}

			data.push_back(possibleNewline);

			char c = 0;

			for (size_t i = 1; (i < decodeSize) && Peek(c); i++) {
				data.push_back(c);
				Next();
			}
		}
	}
	bool ProduceSingleCharacterToken(char c, Token::Type type) {
		tmpToken.type = type;
		tmpToken.data.clear();
		tmpToken.data.push_back(c);
		Produce();
		return true;
	}
	void Produce() {

		tmpToken.line = line;
		tmpToken.column = column;

		if (consumer != nullptr) {
			consumer->Consume(tmpToken);
		}

		if (tmpToken.type == Token::Type::Other) {
			column++;
		} else if (tmpToken.type == Token::Type::Newline) {
			line++;
			column = 1;
		} else {

			auto it = tmpToken.data.cbegin();

			while (it != tmpToken.data.cend()) {

				auto c = *it;

				if (c == '\n') {
					line++;
					column = 1;
					it++;
					continue;
				}

				size_t decodeSize = 1;

				if (!CalculateUtf8Size(c, decodeSize)) {
					column++;
					it++;
					continue;
				}

				while (decodeSize > 0) {
					it++;
					decodeSize--;
				}

				column++;
			}
		}
	}
	bool Peek(char &c) {

		if (lookaheadValid) {
			c = lookahead;
			return true;
		}

		if (reader == nullptr) {
			return false;
		}

		auto readCount = reader->Read(&lookahead, 1);
		if (readCount != 1) {
			return false;
		}

		lookaheadValid = true;
		c = lookahead;
		return true;
	}
	void Next() {
		lookaheadValid = false;
	}
};

} // namespace impl

} // namespace

Lexer *Lexer::Create() {
	return new impl::Lexer;
}

} // namespace cache

} // namespace cmake
