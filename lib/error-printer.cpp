// cmake-cache : A C++ library for reading and writing CMake cache variables.
//
// Copyright (C) 2018  Taylor Holberton
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include <cmake/cache/error-printer.hpp>

#include <cmake/cache/error.hpp>

#include <ostream>

namespace cmake {

namespace cache {

namespace {

namespace impl {

class ErrorPrinter final : public cache::ErrorPrinter {
	std::ostream &output;
	std::string sourcePath;
public:
	ErrorPrinter(std::ostream &output_) : output(output_) {

	}
	~ErrorPrinter() {

	}
	void Handle(const Error &error) override {
		if (!sourcePath.empty()) {
			output << sourcePath;
		} else {
			output << "(unknown origin)";
		}
		output << ':';
		output << error.GetLine();
		output << ':';
		output << error.GetColumn();
		output << ": ";
		output << error.GetDescription();
		output << std::endl;
	}
	void UpdateSourcePath(const char *path) override {
		if (path == nullptr) {
			path = "";
		}
		sourcePath = path;
	}
};

} // namespace impl

} // namespace

ErrorPrinter *ErrorPrinter::Create(std::ostream &output) {
	return new impl::ErrorPrinter(output);
}

} // namespace cache

} // namespace cmake
