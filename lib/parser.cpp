// cmake-cache : A C++ library for reading and writing CMake cache variables.
//
// Copyright (C) 2018  Taylor Holberton
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include <cmake/cache/parser.hpp>

#include <cmake/cache/lexer.hpp>
#include <cmake/cache/node-consumer.hpp>
#include <cmake/cache/node-visitor.hpp>
#include <cmake/cache/token-consumer.hpp>
#include <cmake/cache/token-iterator.hpp>
#include <cmake/cache/token-list.hpp>
#include <cmake/cache/unexpected-token.hpp>
#include <cmake/cache/unused-token.hpp>
#include <cmake/cache/variable-decl.hpp>

#include "token.hpp"

#include <memory>
#include <vector>

namespace cmake {

namespace cache {

namespace {

namespace impl {

class NullToken final : public cache::Token {
	std::string emptyString;
public:
	cache::Token *Copy() const override {
		return new NullToken(*this);
	}
	Token::Type GetType() const noexcept override {
		return Token::Type::None;
	}
	const std::string &GetData() const noexcept override {
		return emptyString;
	}
	size_t GetLine() const noexcept override {
		return 0;
	}
	size_t GetColumn() const noexcept override {
		return 0;
	}
};

class TokenIterator final : public cache::TokenIterator {
	NullToken nullToken;
	std::vector<const cache::Token *>::const_iterator begin, end, current;
public:
	TokenIterator(const std::vector<const cache::Token *> &tokenVec) {
		begin = tokenVec.begin();
		end = tokenVec.end();
		current = begin;
	}
	~TokenIterator() {

	}
	bool AtEnd() const noexcept override {
		return current == end;
	}
	const cache::Token &GetCurrent() const noexcept override {
		if ((current != end) && ((*current) != nullptr)) {
			return *(*current);
		} else {
			return nullToken;
		}
	}
	void Next() noexcept {
		if (current != end) {
			current++;
		}
	}
};

class TokenList final : public cache::TokenList {
	std::vector<const cache::Token *> tokenVec;
public:
	TokenList() {

	}
	~TokenList() {

	}
	void Clear() noexcept {
		tokenVec.clear();
	}
	bool Empty() const noexcept override {
		return tokenVec.empty();
	}
	cache::TokenIterator *CreateIterator() const override {
		return new TokenIterator(tokenVec);
	}
	void PushBack(const cache::Token *token) {
		if (token != nullptr) {
			tokenVec.emplace_back(token);
		}
	}
};

class Parser;

class VariableDecl final : public cache::VariableDecl {
	friend Parser;
	impl::TokenList descriptionLineList;
	impl::Token name;
	impl::Token colon;
	impl::Token type;
	impl::Token equalSign;
	impl::TokenList valueList;
public:
	VariableDecl() {

	}
	~VariableDecl() {

	}
	void Accept(NodeVisitor &visitor) const override {
		visitor.Visit(*this);
	}
	void Reset() {
		descriptionLineList.Clear();
		valueList.Clear();
	}
	const cache::TokenList &GetDescriptionLineList() const noexcept override {
		return descriptionLineList;
	}
	const cache::Token &GetVariableName() const noexcept override {
		return name;
	}
	const cache::Token &GetColon() const noexcept override {
		return colon;
	}
	const cache::Token &GetTypeName() const noexcept override {
		return type;
	}
	const cache::Token &GetEqualSign() const noexcept override {
		return equalSign;
	}
	const cache::TokenList &GetValueList() const noexcept override {
		return valueList;
	}
};

class VariableDeclPool final {
	std::vector<std::unique_ptr<VariableDecl>> variableDeclVec;
public:
	VariableDeclPool() {
	}
	~VariableDeclPool() {
	}
	std::unique_ptr<VariableDecl> Create() {

		if (variableDeclVec.empty()) {
			return std::unique_ptr<VariableDecl>(new VariableDecl);
		}

		auto last = std::move(variableDeclVec.back());
		if (last) {
			last->Reset();
		}

		variableDeclVec.pop_back();

		return last;
	}
	void Destroy(std::unique_ptr<VariableDecl> variableDecl) {
		if (variableDecl) {
			variableDeclVec.push_back(std::move(variableDecl));
		}
	}
};

class UnusedToken final : public cache::UnusedToken {
	const cache::Token &token;
public:
	UnusedToken(const cache::Token &token_) noexcept : token(token_) {

	}
	~UnusedToken() {

	}
	const cache::Token &GetToken() const noexcept override {
		return token;
	}
	void Accept(NodeVisitor &visitor) const override {
		visitor.Visit(*this);
	}
};

class UnexpectedToken final : public cache::UnexpectedToken {
	const cache::Token &token;
public:
	UnexpectedToken(const cache::Token &token_) noexcept : token(token_) {

	}
	~UnexpectedToken() {

	}
	const cache::Token &GetToken() const noexcept override {
		return token;
	}
	void Accept(NodeVisitor &visitor) const override {
		visitor.Visit(*this);
	}
};

class TokenPool final {
	std::vector<std::unique_ptr<impl::Token>> tokenVec;
public:
	TokenPool() {
	}
	~TokenPool() {
	}
	std::unique_ptr<impl::Token> Create() {

		if (tokenVec.empty()) {
			return std::unique_ptr<Token>(new impl::Token);
		}

		auto last = std::move(tokenVec.back());

		tokenVec.pop_back();

		return last;
	}
	void Destroy(std::unique_ptr<impl::Token> token) {
		if (token) {
			tokenVec.push_back(std::move(token));
		}
	}
};

class LookAheadBuffer final : public TokenConsumer {
	TokenPool tokenPool;
	std::vector<std::unique_ptr<impl::Token>> tokenVec;
	Lexer *lexer;
public:
	LookAheadBuffer() {
		lexer = nullptr;
	}
	~LookAheadBuffer() {

	}
	void Erase(size_t count) {

		if (count > tokenVec.size()) {
			count = tokenVec.size();
		}

		for (size_t i = 0; i < count; i++) {
			tokenPool.Destroy(std::move(tokenVec[i]));
		}

		size_t nextSize = tokenVec.size() - count;

		for (size_t i = 0; i < nextSize; i++) {
			auto src = i + count;
			auto dst = i;
			tokenVec[dst] = std::move(tokenVec[src]);
		}

		tokenVec.resize(nextSize);
	}
	impl::Token *GetToken(size_t index) noexcept {

		if (!FillToIndex(index)) {
			return nullptr;
		}

		// Redundant check but safe.
		if (index >= tokenVec.size()) {
			return nullptr;
		}

		return tokenVec[index].get();
	}
	void SetLexer(Lexer &lexer_) {
		lexer = &lexer_;
		lexer->SetTokenConsumer(*this);
	}
protected:
	void Consume(const cache::Token &token) override {

		auto newToken = tokenPool.Create();
		if (!newToken) {
			return;
		}

		*newToken = token;

		tokenVec.emplace_back(std::move(newToken));
	}
	bool FillToIndex(size_t index) {

		if (lexer == nullptr) {
			return false;
		}

		while (index >= tokenVec.size()) {
			if (!lexer->Scan()) {
				return false;
			}
		}

		return true;
	}
};

class Parser final : public cache::Parser {
	LookAheadBuffer lookAheadBuffer;
	NodeConsumer *consumer;
	VariableDeclPool variableDeclPool;
public:
	Parser() {
		consumer = nullptr;
	}
	~Parser() {

	}
	bool Parse() override {

		size_t index = 0;

		const auto *firstToken = lookAheadBuffer.GetToken(index);
		if (firstToken == nullptr) {
			return false;
		}

		index++;

		auto firstTokenType = firstToken->GetType();

		if ((firstTokenType == Token::Type::Newline)
		 || (firstTokenType == Token::Type::Space)
		 || (firstTokenType == Token::Type::Comment)) {
			return ProduceUnusedTokenNode(*firstToken, index);
		} else if ((firstTokenType == Token::Type::Identifier)
		        || (firstTokenType == Token::Type::DescriptionLine)) {
			return ParseVariableDecl(*firstToken, index);
		} else {
			return ProduceUnexpectedTokenNode(*firstToken);
		}

		return false;
	}
	void SetNodeConsumer(NodeConsumer &consumer_) override {
		consumer = &consumer_;
	}
	void SetLexer(Lexer &lexer) override {
		lookAheadBuffer.SetLexer(lexer);
	}
protected:
	bool ParseVariableDecl(const Token &firstToken, size_t &index) {

		auto originalIndex = index;

		auto variableDeclPtr = variableDeclPool.Create();
		if (variableDeclPtr == nullptr) {
			return false;
		}

		auto &variableDecl = *variableDeclPtr;

		if (firstToken.GetType() == Token::Type::DescriptionLine) {

			variableDecl.descriptionLineList.PushBack(&firstToken);

			ContinueParsingDescriptionLines(variableDecl.descriptionLineList, index);

			const auto *variableName = GetToken(index);
			if (variableName == nullptr) {
				index = originalIndex;
				variableDeclPool.Destroy(std::move(variableDeclPtr));
				return false;
			}

			variableDecl.name = *variableName;

			index++;

		} else if (firstToken.GetType() == Token::Type::Identifier) {

			variableDecl.name = firstToken;
		}

		const auto *colonToken = GetToken(index);
		if ((colonToken == nullptr) || (colonToken->GetType() != Token::Type::Colon)) {
			index = originalIndex;
			variableDeclPool.Destroy(std::move(variableDeclPtr));
			return false;
		} else {
			variableDecl.colon = *colonToken;
			index++;
		}

		const auto *typeToken = GetToken(index);
		if ((typeToken == nullptr) || (typeToken->GetType() != Token::Type::Identifier)) {
			index = originalIndex;
			variableDeclPool.Destroy(std::move(variableDeclPtr));
			return false;
		} else {
			variableDecl.type = *typeToken;
			index++;
		}

		const auto *equalSignToken = GetToken(index);
		if ((equalSignToken == nullptr) || (equalSignToken->GetType() != Token::Type::EqualSign)) {
			index = originalIndex;
			variableDeclPool.Destroy(std::move(variableDeclPtr));
			return false;
		} else {
			variableDecl.equalSign = *equalSignToken;
			index++;
		}

		ParseValueList(variableDecl.valueList, index);

		auto result = Produce(variableDecl, index);

		variableDeclPool.Destroy(std::move(variableDeclPtr));

		return result;
	}
	void ContinueParsingDescriptionLines(TokenList &tokenList, size_t &index) {

		for (;;) {

			const auto *newlineToken = lookAheadBuffer.GetToken(index);
			if (newlineToken == nullptr) {
				break;
			} else if (newlineToken->GetType() != Token::Type::Newline) {
				break;
			}

			tokenList.PushBack(newlineToken);

			index++;

			const auto *descToken = lookAheadBuffer.GetToken(index);
			if (descToken == nullptr) {
				break;
			} else if (descToken->GetType() != Token::Type::DescriptionLine) {
				break;
			}

			tokenList.PushBack(descToken);

			index++;

		}
	}
	void ParseValueList(TokenList &tokenList, size_t &index) {

		for (;;) {

			const auto *token = GetToken(index);
			if (token == nullptr) {
				break;
			} else if ((token->GetType() == Token::Type::Newline)
			        || (token->GetType() == Token::Type::Comment)) {
				break;
			}

			tokenList.PushBack(token);

			index++;
		}
	}
	bool ProduceUnusedTokenNode(const cache::Token &token, size_t tokenCount) {

		UnusedToken unusedToken(token);

		return Produce(unusedToken, tokenCount);
	}
	bool ProduceUnexpectedTokenNode(const cache::Token &token) {

		UnexpectedToken unexpectedToken(token);

		return Produce(unexpectedToken, 1);
	}
	bool Produce(const Node &node, size_t tokenCount) {

		if (consumer != nullptr) {
			consumer->Consume(node);
		}

		lookAheadBuffer.Erase(tokenCount);

		return true;
	}
	const cache::Token *GetToken(size_t index) {
		return lookAheadBuffer.GetToken(index);
	}
};

} // namespace impl

} // namespace

Parser *Parser::Create() {
	return new impl::Parser;
}

} // namespace cache

} // namespace cmake
