// cmake-cache : A C++ library for reading and writing CMake cache variables.
//
// Copyright (C) 2018  Taylor Holberton
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include <cmake/cache/string-reader.hpp>

#include <cstring>

namespace cmake {

namespace cache {

StringReader::StringReader(const char *data_) noexcept {
	data = data_;
	size = std::strlen(data_);
	position = 0;
}

StringReader::StringReader(const char *data_, size_t size_) noexcept {
	data = data_;
	size = size_;
	position = 0;
}

StringReader::~StringReader() {

}

size_t StringReader::Read(char *dst_data, size_t dst_size) {

	if (position > size) {
		position = size;
	}

	size_t read_count = dst_size;
	if (read_count > (size - position)) {
		read_count = size - position;
	}

	for (size_t i = 0; i < read_count; i++) {
		dst_data[i] = data[position + i];
	}

	position += read_count;

	return read_count;
}

} // namespace cache

} // namespace cmake
