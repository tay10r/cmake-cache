// cmake-cache : A C++ library for reading and writing CMake cache variables.
//
// Copyright (C) 2018  Taylor Holberton
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include <cmake/cache/engine.hpp>
#include <cmake/cache/variable.hpp>

#include <iostream>
#include <memory>

#include <cstdlib>

int main(int argc, const char **argv) {

	if (argc <= 1) {
		std::cerr << "Must specify at least one variable name." << std::endl;
		return EXIT_FAILURE;
	}

	using namespace cmake::cache;

	std::unique_ptr<Engine> engine(Engine::Create());

	if (!engine->OpenCache()) {
		std::cerr << "Failed to open cache." << std::endl;
		return EXIT_FAILURE;
	}

	for (int i = 1; i < argc; i++) {

		auto variable = engine->Find(argv[i]);

		if (!variable) {
			std::cerr << "Could not find \"" << argv[i] << "\" variable." << std::endl;
			continue;
		}

		std::cout << "The value of \"" << argv[i] << "\" is \"" << variable->GetValue() << '"' << std::endl;
	}

	auto shouldSaveCache = false;

	if (shouldSaveCache && !engine->SaveCache()) {
		std::cerr << "Failed to save cache." << std::endl;
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}
